import React from "react";
import RenderProducts from "./RenderProducts";
import "./Products.css"
import ModalFirst from "../Modal_component/ModalFirst";

class LoadProducts extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isOpen: false,
            currentProductId: null,
            currentProduct: false
        }
    }
    async loadProducts(){
        const response = await fetch("./Product.json")
        const data = await response.json()
        const localCart = JSON.parse(localStorage.getItem("cart"))
        const localFavorite = JSON.parse(localStorage.getItem("favorite"))
        const updatedProducts = this.checkProducts(data.products , localCart , localFavorite)
        this.setState({...this.state, products: updatedProducts})

    }
    async componentDidMount() {
        if (!localStorage.getItem("cart") || !localStorage.getItem("favorite"))  {
           localStorage.setItem("cart", JSON.stringify([]))
           localStorage.setItem("favorite", JSON.stringify([]))
        }
        await this.loadProducts()
    }
    checkProducts(products, cart, favorite){
        const updatedProducts = products.map(product =>{
            const productInCart = cart.some(({id}) => (id === product.id))
            const productInFavorite = favorite.some(({id}) => (id === product.id))
            return {...product, 
                isInCart: productInCart, 
                isInFavorites: productInFavorite}
        })
        return updatedProducts
    }
    handleModalOpen(id){
        if(id){
            this.setState((state) => ({
                ...state,
                isOpen: !state.isOpen,
                currentProductId: id,
            }))
        }else{
            this.setState((state) => ({
                ...state,
                isOpen: !state.isOpen,
                currentProductId: null,
            }))
        }
    }
    addProductToCart(id){
        const cart = JSON.parse(localStorage.getItem("cart"))
        const product = this.state.products.find(item => (item.id === id))
        cart.push(product)
        localStorage.setItem("cart", JSON.stringify(cart))
        this.setState((state) => ({
            ...state,
            isOpen: !state.isOpen,
            currentProductId: null,
        }))
    }
    addProductToFavorite(id){     
        const favorite = JSON.parse(localStorage.getItem("favorite"))
        const product = this.state.products.find(item => (item.id === id))          
        favorite.push(product)
        localStorage.setItem("favorite", JSON.stringify(favorite))

        const favoriteList = this.state.products.map((item) => {   
            if(item.id === id){
                item.isInFavorites = true
            }
            return item
        })
        this.setState((state) => ({
            ...state,
            currentProduct: !state.currentProduct,
            currentProductId: id,
            products: favoriteList,
        }))
    }

    removeProductFromFavorite(id){
        const favorite = JSON.parse(localStorage.getItem("favorite"))
        favorite.map((item, index) => {
            if(id === item.id){
                favorite.splice(index, 1)
            }
            return favorite
        })
        localStorage.setItem("favorite", JSON.stringify(favorite))
        const product = this.state.products.map((item) => {
            if(id === item.id){
                item.isInFavorites = false
            }
            return item
        })
        this.setState((state) => ({
            ...state,
            currentProduct: !state.currentProduct,
            products: product,
        }))
        console.log(favorite);
        console.log(product);
    }

    render(){
        return(
            <div className="products"> =s
                <RenderProducts product = {this.state.products}
                    handleModalOpen = {this.handleModalOpen.bind(this)}
                    addProductToFavorite = {(id) => {this.addProductToFavorite(id)}}
                    removeProductFromFavorite = {(id) => {this.removeProductFromFavorite(id)}}
                />
                <ModalFirst             
                id = {this.state.currentProductId}
                isOpen = {this.state.isOpen}
                header = {"Хотите добавить в корзину?"}
                onCancel = {() =>{this.handleModalOpen()}}
                onSubmit = {(id) =>{this.addProductToCart(id)}}
                actionsGood = {"Да"}
                actionsBad = {"Нет"} 
                />
            </div>
        )
    }
}

export default LoadProducts;