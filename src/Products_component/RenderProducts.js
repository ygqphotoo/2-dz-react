import React, {Component} from "react";
import PropTypes from 'prop-types';
import Button from "../Button_component/Button";
import {AiOutlineStar} from "react-icons/ai"

class RenderProducts extends Component{
    constructor(props) {
        super(props);
            this.products = props
        }
    render() {
        return (
            <>
                {
                    this.props.product &&
                    this.props.product.map((product, index) => {
                        return (
                            <div key={index.toString()} className="product_position">
                                <div className="products_set">
                                    <p className="product_name">{product.name}</p>
                                    <p className="product_price">{product.price}</p>
                                    <img src={product.url} alt="product_image"/>
                                    <p className="product_article">Article: {product.article}</p>
                                    <p className="product_color">Color: {product.color}</p>
                                    <Button className="propsBtn"
                                        onClick = {() => {this.props.handleModalOpen(product.id)}}
                                        text = {"Add to cart"}>
                                    </Button>
                                    <AiOutlineStar
                                     onClick = {() => product.isInFavorites ? this.props.removeProductFromFavorite(product.id) : this.props.addProductToFavorite(product.id)}
                                     className = {product.isInFavorites ? "favorite": ""}/>
                                </div>
                            </div>
                        );
                    })
                }
            </>
        )
    }
}

RenderProducts.propTypes = {
    products: PropTypes.array,
    name: PropTypes.string,
    price: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
}

export default RenderProducts;