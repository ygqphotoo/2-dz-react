import React from "react";
import "../ModalStyles/ModalFirstStyle.css"

class ModalFirst extends React.Component{
    render()
    {
        return(
            <>
                {this.props.isOpen &&
                    <>
                        <div className="modal">
                            <div className="overflow" onClick={this.props.onCancel}></div>
                            <div className="modal_content" >
                                <button className="closeActive_first" onClick={this.props.onCancel} >X</button>
                                <h1 className="modal__title_first">{this.props.header}</h1>
                                <p className="modal__text_first">{this.props.text}</p>
                                <div className="btn__position">
                                    <button className="super_first" onClick={() =>{
                                        this.props.onSubmit(this.props.id)}}>
                                            {this.props.actionsGood}</button>
                                    <button className="bad_first" onClick={this.props.onCancel}>{this.props.actionsBad}</button>
                                </div>
                            </div>
                        </div>
                    </>
                }
            </>
        )
    }
}

export default ModalFirst;